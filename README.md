# Burger-Builder

Find this gem in action at http://DatGoodBurger.surge.sh/

This is an implementation of the burger builder app found in Udemy course - React 16.6 The Complete React Guide by Maximilian Schwarzmuller.


## Surge Deployment
1. Surge.sh is sweet but you can only deploy something with index.html at the root. No worries because...
  1. `cd <your tremendous react project root where you've been typing 'npm start' all this time for dev tinkering>`
  2. `npm run build` This will generate an optimized production build in a folder labeled build at your project root
2. `cd <your tremendous react project root>/build`
3. `surge` Surge deploys the folder you currently in. This means /build folder is the root from Surge's perspective and it has the index.html file it so desperately needs.
4. Answer the prompts to login
5. When surge prompts for a domain, edit that to your hearts content per the pricing criteria you happen to have.
6. Visit your tremendous deployed react project at the domain you specified in the terminal and tell all your friends.
