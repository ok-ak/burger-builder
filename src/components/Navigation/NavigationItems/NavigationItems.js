import React from 'react';
import classes from './NavigationItems.module.css';
import NavigationItem from './NavigationItem/NavigationItem';

const navigationItems = (props) => {
  return (
    <ul className = {classes.NavigationItems}>
      <NavigationItem hyperlink={'/'} active>Burger Builder</NavigationItem>
      <NavigationItem hyperlink={'/'}>Checkout</NavigationItem>
    </ul>
  )
}

export default navigationItems;