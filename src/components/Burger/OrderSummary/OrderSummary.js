import React from 'react';
import Aux from '../../../hoc/Aux/Aux';
import Button from '../../UI/Button/Button';

const orderSummary = (props) => {
  const ingredientSummary = Object.keys(props.ingredients)
  .map((key) => {
    return (
      <li key={key}>
        <span style={{textTransform: 'capitalize'}}>{key + ':' + props.ingredients[key]}</span>
      </li>
    )
  });

  return (
    <Aux>
      <h3>Your Order</h3>
      <p>A delicious burder with the following ingredients:</p>
      <ul>
        {ingredientSummary}
      </ul>
      <p><strong>{'Total: ' + props.totalPrice.toFixed(2)}</strong></p>
      <p>Continue to Checkout?</p>
      <Button btnType='Danger' clicked={props.cancelOrder}>CANCEL</Button>
      <Button btnType='Success' clicked={props.makeOrder}>CONTINUE</Button>
    </Aux>
  )
};

export default orderSummary;